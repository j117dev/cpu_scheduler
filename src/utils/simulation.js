import * as _ from 'lodash';

/**
 * This function will generate a randomly generated unique ID
 * @returns {String} GUID
 */
export const generateGuid = () => {
    return Math.random()
        .toString(36)
        .substring(8);
};

/**
 * This function generates a random number between 1 and 100
 * @returns {Number} A random number
 */
export const generateRandomBurst = () => {
    return Math.floor(Math.random() * 100) + 1;
};

/**
 * Checks whether an input is valid
 * @param {Number} input A number input
 */
export const isValidInput = input => {
    return input === '' || Number(input) > 0 && Number(input) <= 10000;
};

/**
 * Checks whether an input is empty
 * @param {Number} input A number input
 */
export const validateInput = input => {
    return input !== '' && Number(input) > 0;
};

export const calculateAverage = (processMap, key) => {
    //convert to array
    const array = _.values(processMap);
    //get mean by key
    const mean = _.meanBy(array, (process) => Number(process[key]));
    //round two places
    return mean.toFixed(2);
};