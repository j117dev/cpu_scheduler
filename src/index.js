import { app, BrowserWindow, ipcMain } from 'electron';
import installExtension, {
    REACT_DEVELOPER_TOOLS,
} from 'electron-devtools-installer';
import { enableLiveReload } from 'electron-compile';


if (require('electron-squirrel-startup')) app.exit();


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, statWindow;

const isDevMode = process.execPath.match(/[\\/]electron/);

if (isDevMode) enableLiveReload({ strategy: 'react-hmr' });

const createStatWindow = () => {
    statWindow = new BrowserWindow({
        width: 800,
        height: 250,
        show: false,
        // frame: false,
    });
    statWindow.loadURL(`file:///${__dirname}/public/index.html#/statWindow`);

    //Override onClose event, only hide window. This way the instance is created
    //once rather than continuously created and destroyed
    statWindow.on('close', (e) => {
        e.preventDefault();
        statWindow.hide();
    });
    if (!isDevMode) {
        statWindow.setMenu(null);
    }
};


const createWindow = async () => {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
    });

    // and load the index.html of the app.
    mainWindow.loadURL(`file://${__dirname}/public/index.html`);
    // Open the DevTools.
    if (isDevMode) {
        await installExtension(REACT_DEVELOPER_TOOLS);
        mainWindow.webContents.openDevTools();
        statWindow.webContents.openDevTools();
    }

    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.

        statWindow = null;
        mainWindow = null;
        app.exit();
    
    });
    if (!isDevMode) {
        mainWindow.setMenu(null);
    }
};


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow();
  createStatWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
        createStatWindow();
    }
});

//Listen for an IPC call from a renderer process, show or hide browser window
ipcMain.on('show-stats-window', (e, data) => {
    statWindow.setPosition(data.coordinates.x, data.coordinates.y);
    data.action === 'open' ? statWindow.show() : statWindow.hide();
});

//Send simulation results to data window process
ipcMain.on('send-results-to-stats-window', (e, data) => {
    statWindow.webContents.send('action-update-data', data);
});
