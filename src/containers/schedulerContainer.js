import React from 'react';
import styled from 'styled-components';
import LeftNav from '../components/navigation/leftNav';
import Header from '../components/navigation/header';
import { AppContextProvider } from '../contexts/app/app';
import { FIRST_COME_FIRST_SERVE, SHORTEST_JOB_FIRST, ROUND_ROBIN } from '../types/cpu_scheduler';
import SimulationContainer from './simulationContainer';

const BodyWrapper = styled.div`
    display: flex;
`;

const menu = [
    {
        label: 'First Come First Serve',
        id: FIRST_COME_FIRST_SERVE
    },

    { 
        label: 'Shortest Job First',
        id: SHORTEST_JOB_FIRST
    },
    // { 
    //     label: 'Round Robin',
    //     id: ROUND_ROBIN
    // },
];

export default function SchedulerContainer() {
    return (
        <div>
                <BodyWrapper>
                    {/*Navigation Elements*/}
                    <Header 
                        title={'CPU Scheduler'}
                    />
                    <LeftNav 
                        mode={'SCHEDULER'}
                        menuItems={menu} />
                    {/*Table*/}
                    <SimulationContainer />
                </BodyWrapper>
        </div>
    );
}
