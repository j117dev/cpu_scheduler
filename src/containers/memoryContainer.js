import React from 'react';
import styled from 'styled-components';
import LeftNav from '../components/navigation/leftNav';
import Header from '../components/navigation/header';
import { FIRST_FIT } from '../types/memory_manager';
import Memory from '../components/memory';

const BodyWrapper = styled.div`
    display: flex;
`;

const menu = [
    {
        label: 'First Fit',
        id: FIRST_FIT,
    },
];

export default function MemoryContainer() {
    return (
        <BodyWrapper>
            {/*Navigation Elements*/}
            <Header 
                title={'Memory Manager'}
            />
            <LeftNav
                menuItems={menu}
                mode={'MEMORY'}
            />
            {/*Table*/}
                <Memory />
        </BodyWrapper>
    );
}
