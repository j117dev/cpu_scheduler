import React from 'react';
import { COLORS } from '../theme/colors';
import styled from 'styled-components';
import Simulation from '../components/simulation';

const TableInner = styled.div`
    margin: 25px;
    height: 100%;
    .MuiTableRow-root th {
        background: ${COLORS.grayPrimary};
        color: white;
    }
    .MuiPaper-root {
        box-shadow: 1px 3px 20px black;
    }
    div:first-of-type {
        height: calc(100% - 75px);
    }
   
    .MuiTablePagination-root:last-child {
        bottom: 64px;
        position: absolute;
        right: 24px;
    }
`;

const TableWrapper = styled.div`
    background: ${COLORS.bluePrimary};
    height: 100vh;
    width: 100vw;
    position: absolute;
    margin-top: 50px;
`;


export default function SimulationContainer(props) {
    return (
        <TableWrapper>
            <TableInner>
                <Simulation />
            </TableInner>
        </TableWrapper>
    );
}
