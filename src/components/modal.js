import React, { useState, useContext, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { AppContext } from '../contexts/app/appContext';
import { COLORS } from '../theme/colors';

export default function FormModal(props) {
    const appContext = useContext(AppContext);

    return (
        <div style={{ position: 'absolute', height: '100vh' }}>
            <Dialog open={appContext.isModalOpen}>
                <DialogTitle>{props.title}</DialogTitle>
                <DialogContent>{props.children}</DialogContent>
                <DialogActions>
                    <Button
                        onClick={() => appContext.setIsModalOpen(false)}
                        style={{ color: COLORS.graySecondary }}
                    >
                        Close
                    </Button>
                    <Button
                        disabled={props.submitDisabled}
                        onClick={props.onSubmit}
                        style={{ color: !props.submitDisabled && COLORS.bluePrimary }}
                    >
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

FormModal.defaultProps = {
    open: false,
    onSubmit: () => {},
};
