import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import FormModal from './modal';
import { AppContext } from '../contexts/app/appContext';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import MemoryMeter from './memory/memoryMeter';
import { isValidInput, generateGuid } from '../utils/simulation';
import Button from '@material-ui/core/Button';

const BodyWrapper = styled.div`
    margin-top: 100px;
`;
const Overlay = styled.div`
    height: 100vh;
    width: 100vw;
    background: black;
    opacity: 0.6;
    position: absolute;
    z-index: 2;
    top: 0px;
    left: 0px;
`;
export default function Memory() {
    const appContext = useContext(AppContext);
    const [formData, setFormData] = useState({
        total_memory: 4096,
        os_memory: 400,
    });
    const [addProcess, setAddProcess] = useState('');
    const [processCount, setProcessCount] = useState(1);
    const [error, setError] = useState(false);
    function handleChange(e, key, value) {
        setFormData(Object.assign({}, formData, { [key]: Number(value) }));
    }
    
    const handleAdd = () => {
        if (addProcess) {
            appContext.addMemoryData({
                name: `P${processCount}`,
                size: Number(addProcess),
                pid: generateGuid()
            });
            setProcessCount(processCount+1);
        }
    }

    const handleCompact = () => {
        appContext.compactMemory();
    }

    const handleClose = (pid) => {
        console.log('CLOSE', pid);
        appContext.removeMemory(pid);
    }

    return (
        <BodyWrapper style={{display: appContext.memoryData.os_memory > 0 ? 'flex' : 'none'}}>
            <MemoryMeter
                total_memory={appContext.memoryData.total_memory}
                os_memory={appContext.memoryData.os_memory}
                processes={appContext.memoryData.processes}
                handleClose={handleClose}
            />
            <div style={{ position: 'absolute', bottom: 0, left: 8 }}>
                <TextField
                    style={{bottom: 10}}
                    autoFocus
                    id='add_process'
                    placeholder='Size'
                    type='number'
                    label='Add Process'
                    error={appContext.memoryData.error}
                    helperText={appContext.memoryData.error && 'Not enough storage space'}
                    onChange={e =>
                        isValidInput(e.target.value) &&
                        e.target.value !== '' &&
                        setAddProcess(e.target.value)
                    }
                    value={addProcess.size}
                />
                <Button onClick={handleAdd}>Add</Button>
            </div>
            {appContext.isMenuOpen && (
                <Overlay onClick={() => appContext.setIsMenuOpen(false)} />
            )}
            <FormModal
                title={'Configure Memory'}
                onSubmit={() => {
                    if (formData.os_memory < formData.total_memory) {
                        appContext.setMemory(formData);
                        appContext.setIsModalOpen(false);
                        setError(false);
                    }
                    else {
                        setError(true);
                    }
                }}
            >
                <FormGroup column='true'>
                    <TextField
                        autoFocus
                        id='total_memory'
                        placeholder='Total Memory'
                        type='number'
                        label='Total Memory'
                        onChange={e =>
                            isValidInput(e.target.value) &&
                            e.target.value !== '' &&
                            handleChange(e, 'total_memory', e.target.value)
                        }
                        value={formData.total_memory}
                        error={error}
                    />
                    <TextField
                        autoFocus
                        id='os_memory'
                        placeholder='OS Memory'
                        type='number'
                        label='OS Memory'
                        onChange={e =>
                            isValidInput(e.target.value) &&
                            e.target.value !== '' &&
                            handleChange(e, 'os_memory', e.target.value)
                        }
                        error={error}
                        value={formData.os_memory}
                    />
                </FormGroup>
            </FormModal>
            <Button style={{position: 'absolute', right: 8, bottom: 8}} onClick={handleCompact}>Compact</Button>
        </BodyWrapper>
    );
}
