import React from 'react';
import { COLORS } from '../../theme/colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'
import WelcomeAnimation from './welcomeAnimation';

const WelcomeWrapper = styled.div`
    background: ${COLORS.bluePrimary}
    height: 100vh;
    width: 100vw;
    display: grid;
    align-items: center;
    grid-template-rows: 1fr 100px;
    div {
        display: grid;
    }
    a {
        justify-self: center;

    }
    .button_container {
        display: grid;
        align-items: center;
        justify-content: center;
        grid-auto-flow: column;
        grid-gap: 5px;
        button {
            color: white;
        }
        button:hover {
            background: ${COLORS.graySecondary};
        }
    }
`;

export default function SchedulerContainer() {
    return (
        <WelcomeWrapper>
            <div>
                <WelcomeAnimation />
            </div>
            <div className='button_container'>
                <Link to='/cpuScheduler'>
                    <Button variant='outlined'>CPU Scheduler</Button>
                </Link>
                <Link to='/memoryManager'>
                    <Button variant='outlined'>Memory Manager</Button>
                </Link>
            </div>
        </WelcomeWrapper>
    );
}
