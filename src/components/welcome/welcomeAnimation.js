import React, { useEffect, useState } from 'react';
import * as THREE from 'three';
import styled from 'styled-components';

const AnimationWrapper = styled.div`
    display: grid;
    #container {
        align-self: center;
        justify-self: center;
        display: grid;
    }
`;

export default function WelcomeAnimation() {     
    let 
        sphere,
        outerPoints,
        innerPoints;
    let camera, scene, renderer;

    useEffect(() => {
        init();
    }, []);

    /**
     * This function will initialize the animation and append it to the DOM
     */
    const init = () => {
        camera = new THREE.PerspectiveCamera(45, 1, 0.1, 1000);
        camera.position.z = 150;
        scene = new THREE.Scene();

        createPoints();
        createSphere();
        
        renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        renderer.setSize(350, 350);
        renderer.setClearColor(0x000000, 0); // the default
        document.getElementById('container').appendChild(renderer.domElement);
        animate();
    };

    /**
     * This function will rotate the appropriate objects accordingly
     */
    const animate = () => {
        requestAnimationFrame(animate);
        outerPoints.rotation.x += 0.01;
        outerPoints.rotation.y += 0.02;

        innerPoints.rotation.x += 0.02;
        innerPoints.rotation.y += 0.02;

        sphere.rotation.y += 0.03;
        renderer.render(scene, camera);
    };

    /**
     * This function will create the inner and outer points
     */
    const createPoints = () => {
        const geometry = new THREE.BoxGeometry(50, 50, 50);
        const material = new THREE.PointsMaterial({ color: 0xf3ffe2 });
        outerPoints = new THREE.Points(geometry, material);
        innerPoints = new THREE.Points(geometry, material);
        scene.add(outerPoints);
        scene.add(innerPoints);
    } 

    /**
     * This function will create a sphere object and add it to the scene
     */
    const createSphere = () => {
        const radius = 30;
        const segments = 15;
        const rings = 20;

        const sphereGeometry = new THREE.SphereGeometry(
            radius,
            segments,
            rings
        );
        const sphereMaterial = new THREE.MeshBasicMaterial({
            color: 0x343a40,
            wireframe: true,
        });

        sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
        scene.add(sphere);
    };

    return (
        <AnimationWrapper>
            <div id="container"></div>
        </AnimationWrapper>
    );
}
