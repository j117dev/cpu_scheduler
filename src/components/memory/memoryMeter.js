import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import { COLORS } from '../../theme/colors';

const BodyWrapper = styled.div`
    display: flex;
    align-content: center;
    justify-content: center;
    left: 0px;
    position: absolute;
    width: 100%;
    p {
        position: absolute;
        bottom: -32px;
        right: 16px;
    }
`;

const MeterOuter = styled.div`
    height: 100px;
    background: green;
    width: 100%;
    margin: 0px 10px 0px 10px;
    display: flex;
    box-shadow: 5px 4px 16px 0px slategrey;
`;

const OSBlock = styled.div`
    background: ${COLORS.bluePrimary};
    color: white;
    width: ${props => props.os_size};
    height: 100%;
    justify-content: center;
    align-content: center;
    display: grid;
    opacity: .5;
    p {
        position: absolute;
        bottom: 8px;
        left: 16px;
        font-size: 10px;
    }
`;

const MemBlock = styled.div`
    background: ${COLORS.bluePrimary};
    width: ${props => props.size};
    position: absolute;
    height: 100%;
    left: calc(${props => props.x});
    border-left: 1px solid rgba(170, 170, 170, .5);
    padding: 8px;
    margin-left: 1px;
    color: white;
    top: 0;
    div {
        display: flex;
        justify-content: space-between;
    }
    span {
        cursor: pointer;
    }
    p {
        position: absolute;
        bottom: 8px;
        right: 8px;
        font-size: 10px;
    }
`;

function calculatePercentage(num, total) {
    return `${(num / total) * 100}%`;
}

export default function MemoryMeter(props) {
    const { processes, os_memory, total_memory } = props;
    return (
        <BodyWrapper>
            <MeterOuter>
                <div style={{ background: `${COLORS.grayPrimary}`, width: '100%' }}>
                    <OSBlock
                        os_size={() => calculatePercentage(os_memory, total_memory)}
                    >
                        OS
                        <p>{os_memory}KB</p>
                        </OSBlock>
                    {processes.map((process, i) => (

                        <MemBlock
                            x={() => calculatePercentage(process.x, total_memory)}
                            size={() => calculatePercentage(process.size, total_memory)}
                        >
                            <div>
                                {process.name}
                                <span onClick={() => props.handleClose(process.pid)}>X</span>
                                
                            </div>
                            <p>{process.size}KB</p>
                        </MemBlock>
                    ))}
                </div>
            </MeterOuter>
                    <p>{total_memory}KB</p>
        </BodyWrapper>
    );
}
