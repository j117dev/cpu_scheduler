import React, { useContext, useEffect, useState, isValidElement } from 'react';
import TableBody from '@material-ui/core/TableBody';
import { columns } from './columns';
import TableRow from '@material-ui/core/TableRow';
import { AppContext } from '../../contexts/app/appContext';
import TableCell from '@material-ui/core/TableCell';
import TextField from '@material-ui/core/TextField';
import * as _ from 'lodash';
import { validateInput } from '../../utils/simulation';

export default function Body(props) {
    const { page, rowsPerPage, simulator } = props;
    const appContext = useContext(AppContext);
    const [hasError, setHasError] = useState(null);

    /**
     * This function will update the burst for a given process
     * @param {String} pid The process id
     * @param {Number} val The row which was clicked
     */
    const handleUpdate = (pid, val) => {
        if (pid === hasError) return;
        simulator.setBurst(pid, val);
        appContext.setRerender(!appContext.reRender);
    };

    /**
     * This function will uvalidate the text input
     * @param {Object} e Click event
     * @param {Number} pid The process id of the row whose
     * burst is being edited
     */
    const handleChange = (e, pid) => {
        validateInput(e.target.value) ? setHasError(null) : setHasError(pid);
    };

    return (
        <TableBody>
            {Array.from(simulator.processMap.values())
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((process, i) => {
                    return (
                        <TableRow
                            hover
                            role="checkbox"
                            tabIndex={-1}
                            key={`checkbox_${i}`}
                        >
                            <TableCell
                                align="center"
                                key={`edit_${i}`}
                            ></TableCell>
                            {columns.map(column => {
                                const value = process[column.id];
                                return props.editMode &&
                                    column.id === 'burst' ? (
                                    <TableCell
                                        key={column.id}
                                        align={'center'}
                                        size="small"
                                    >
                                        <TextField
                                            style={{ width: 75, margin: 0 }}
                                            id="outlined-basic"
                                            margin="normal"
                                            variant="filled"
                                            type="number"
                                            error={hasError === process.pid}
                                            onBlur={(e, val) =>
                                                handleUpdate(
                                                    process.pid,
                                                    e.target.value
                                                )
                                            }
                                            onChange={e =>
                                                handleChange(e, process.pid)
                                            }
                                            defaultValue={
                                                value
                                                    ? column.format &&
                                                      typeof value === 'number'
                                                        ? column.format(value)
                                                        : value
                                                    : typeof value === 'undefined' ? '-' : value
                                            }
                                        />
                                    </TableCell>
                                ) : (
                                    <TableCell
                                        key={column.id}
                                        align={'center'}
                                        style={{ padding: 24 }}
                                    >
                                        {value
                                            ? column.format &&
                                              typeof value === 'number'
                                                ? column.format(value)
                                                : value
                                            :  typeof value === 'undefined' ? '-' : value}
                                    </TableCell>
                                );
                            })}
                        </TableRow>
                    );
                })}
        </TableBody>
    );
}
