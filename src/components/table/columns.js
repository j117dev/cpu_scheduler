/**
 * An array of columns to be mapped in a table
 */
export const columns = [
    {
        id: 'name',
        label: 'Process',
    },
    {
        id: 'burst',
        label: 'Burst',
    },
    {
        id: 'wait_time',
        label: 'Wait Time',
    },
    {
        id: 'turnaround_time',
        label: 'Turnaround Time',
    },
];