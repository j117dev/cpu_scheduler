import React from 'react';
import TablePagination from '@material-ui/core/TablePagination';

/**
 * This component will be used to handle table pages
 * @param {Object} props 
 */
export default function Pagination(props) {
    return (
        <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={props.size}
            rowsPerPage={props.rowsPerPage}
            page={props.page}
            backIconButtonProps={{
                'aria-label': 'previous page',
            }}
            nextIconButtonProps={{
                'aria-label': 'next page',
            }}
            onChangePage={props.handleChangePage}
            onChangeRowsPerPage={props.handleChangeRowsPerPage}
        />
    );
}
