import React, { useState, useContext } from 'react';
import { COLORS } from '../../theme/colors';
import styled from 'styled-components';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { AppContext } from '../../contexts/app/appContext';

const LeftNavWrapper = styled.div`
z-index: 3;
height: 100vh;
margin-top: 50px;
min-width: 200px;
@keyframes slidein {
  from {transform: translateX(-100%)}
  to {
    display: none;
    transform: translateX(0%)
  }
}

@keyframes slideout {
  from {transform: translateX(0%)}
  to {transform: translateX(-100%)}}

  @keyframes fadein {
    from {opacity: 1}
    to {opacity: .4}
  }

  .MuiButtonBase-root {
    padding: 0px !important;
  }

.MuiButtonBase-root:hover {
  border-right 1px solid white;
  cursor: pointer;
  background: ${COLORS.grayPrimary};
  animation: fadein .3s ease-in-out;
  opacity: .4;
}

.MuiListItemText-root {
  padding: 16px;
}

ul {
  padding-top: 0px;
}
background: ${COLORS.graySecondary};
div {
  background: ${COLORS.grayPrimary};
  color: #e3e3e3;
};

`;
export default function LeftNav(props) {
    const appContext = useContext(AppContext);

    const handleClick = (item) => {
      switch(props.mode) {
        case 'SCHEDULER':
            appContext.simulatorController.clearSimulation();
            appContext.setActiveAlgorithm(item.id);
            appContext.setIsMenuOpen(false);
            appContext.setIsModalOpen(true);
          break;
        case 'MEMORY':
            appContext.setIsMenuOpen(false);
            appContext.setIsModalOpen(true);
          break;
      }
    }

    return (
        <LeftNavWrapper
            style={{
                transform: appContext.isMenuOpen
                    ? 'translateX(0%)'
                    : 'translateX(-100%)',
                animation: appContext.isMenuOpen
                    ? 'slidein .5s'
                    : 'slideout .5s',
            }}
            showMenu={appContext.isMenuOpen}
        >
            <List>
                {props.menuItems.map((item, i) => (
                    <ListItem divider button key={item.id}>
                        {/*Set Active Algorithm onClick based on algorithm ID*/}
                        <ListItemText
                            onClick={() => handleClick(item) }
                            primary={item.label}
                        />
                    </ListItem>
                ))}
            </List>
        </LeftNavWrapper>
    );
}
