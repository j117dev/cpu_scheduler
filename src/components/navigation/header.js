import React, { useContext } from 'react';
import styled from 'styled-components';
import { COLORS } from '../../theme/colors';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { AppContext } from '../../contexts/app/appContext';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom';
import { labelMap } from '../../types/cpu_scheduler';
const HeaderWrapper = styled.div`
    .MuiToolbar-root {
        display: grid;
        grid-template-columns: 32px 180px 1fr;
        grid-gap: 10px;

        a {
            justify-self: right;
        }
    }
    div {
        box-shadow: 0px 1px 20px 0px black;
    }
    .title-container {
        display: grid;
        grid-template-columns: 1fr 1fr;
        width: 280px;
        justify-content: center;
        align-items: center;
        justify-items: center;
        box-shadow: none !important;
        p {
            justify-self: left;
        }
    }
`;
export default function Header(props) {
    const appContext = useContext(AppContext);

    /**
     * This function toggles the menu state
     */
    const handleMenuClick = () => {
        appContext.setIsMenuOpen(!appContext.isMenuOpen);
    };

    return (
        <HeaderWrapper>
            <AppBar
                style={{
                    zIndex: 3,
                    position: 'absolute',
                    width: '100vw',
                }}
                position='static'
                color={'white'}
            >
                <Toolbar variant='dense'>
                    <IconButton
                        edge='start'
                        color='inherit'
                        aria-label='menu'
                        onClick={handleMenuClick}
                    >
                        <MenuIcon />
                    </IconButton>
                    {appContext.activeAlgorithm ? (
                        <div className={'title-container'}>
                            <Typography variant='h6'>{`${props.title}:`}</Typography>
                            <p>{labelMap[appContext.activeAlgorithm]}</p>
                        </div>
                    ) : (
                        <Typography variant='h6'>{`${props.title}`}</Typography>
                    )}
                    <Link to='/'>
                        <Icon>home</Icon>
                    </Link>
                </Toolbar>
            </AppBar>
        </HeaderWrapper>
    );
}
