import React, { useContext, useState, useEffect } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import styled from 'styled-components';
import { AppContext } from '../contexts/app/appContext';
import FormModal from './modal';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { FIRST_COME_FIRST_SERVE, SHORTEST_JOB_FIRST } from '../types/cpu_scheduler';
import * as _ from 'lodash';
import {
    isValidInput,
    validateInput,
    calculateAverage,
} from '../utils/simulation';
import { columns } from './table/columns';
import Body from './table/body';
import Pagination from './table/pagination';
import { COLORS } from '../theme/colors';
import EditIcon from '@material-ui/icons/Edit';
const { remote } = window.require('electron');
const Overlay = styled.div`
    height: 100vh;
    width: 100vw;
    background: black;
    opacity: 0.6;
    position: absolute;
    z-index: 2;
    top: 0px;
    left: 0px;
`;

const BottomPanel = styled.div`
    border-top: 1px solid ${COLORS.graySecondary};
    background: ${COLORS.blueSecondary};
    position: absolute;
    bottom: 50px;
    height: 60px;
    left: 0px;
    width: 100vw;
    display: flex;
    justify-content: space-between;
    button {
        font-size: 12px;
        box-shadow: 1px 1px 6px black;
        color: white;
        background: ${COLORS.grayPrimary};
        margin: 10px;
        &:first-of-type {
            margin-left: 24px;
        }
        &:nth-of-type(2) {
            margin-right: 24px;
        }
        &:hover {
        }
    }
`;

const TableWrapper = styled.div`
    .root {
        width: 100%;
    }
    .inner {
        max-height: 440;
        overflow: auto;
        .edit-icon {
            position: absolute;
            top: 40px;
            left: 64px;
            z-index: 2;
            cursor: pointer;
            color: white;
        }
    }
    .MuiTablePagination-actions button {
        bottom: 2px;
    }
`;

export default function ProcessTable() {
    const [isEditMode, setIsEditMode] = useState(false);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const appContext = useContext(AppContext);
    const [formData, setFormData] = useState({
        number_of_processes: '',
    });
    const { processMap, setProcessMap, simulatorController } = appContext;
    const [val, setVal] = useState(false);

    //Clear number of processes on open of modal
    useEffect(() => {
        appContext.isModalOpen && setFormData({ number_of_processes: '' });
    }, [appContext.isModalOpen]);

    //update simulatorController when local state changes
    useEffect(() => {
        simulatorController.setNumberOfProcesses(formData.number_of_processes);
    }, [formData.number_of_processes]);

    //Trigger a rerender when process map changes
    useEffect(() => {
        setVal(!val);
    }, [processMap]);

    /**
     * This function will update state based on the parameters provided
     * @param {Object} e The event object
     * @param {String} parentKey The key of the parent to update
     * @param {String} value The value of the key to update
     */
    const handleChange = (e, parentKey, value) => {
        setFormData(Object.assign({}, formData, { [parentKey]: value }));
    };

    /**
     *
     * @param Object e Click event
     * @param {Number} newPage The page number you wish to display
     */
    const handleChangePage = (e, newPage) => {
        setPage(newPage);
    };

    /**
     * This function will update the allowed rows per page
     * @param {Object} e Click event
     */
    const handleChangeRowsPerPage = e => {
        setRowsPerPage(+e.target.value);
        setPage(0);
    };

    /**
     * This function will generate a mapping of generated processIDs to process objects
     */

    const openDataView = () => {
        appContext.toggleDataView('open');
        appContext.sendDataToView({
            turnaroundAverage: simulatorController.getAverages().turnaround_time_average,
            waitAverage: simulatorController.getAverages().wait_time_average,
            processArray: Array.from(simulatorController.processMap.values())
        });
        //force refresh
        setVal(val => !val);
    };

    return (
        <TableWrapper>
            <Paper className={'root'}>
                {appContext.isMenuOpen && (
                    <Overlay onClick={() => appContext.setIsMenuOpen(false)} />
                )}
                <div className={'inner'}>
                    <EditIcon
                        className={'edit-icon'}
                        onClick={e => setIsEditMode(!isEditMode)}
                    />
                    <Table stickyHeader>
                        {/*Map Column Headers*/}
                        <TableHead>
                            <TableRow>
                                <TableCell />
                                {columns.map(
                                    column =>
                                        column && (
                                            <TableCell
                                                align="center"
                                                key={column.id}
                                            >
                                                {column.label}
                                            </TableCell>
                                        )
                                )}
                            </TableRow>
                        </TableHead>
                        {/*Map data*/}
                        <Body
                            simulator={simulatorController}
                            page={page}
                            rowsPerPage={rowsPerPage}
                            editMode={isEditMode}
                        />
                    </Table>
                </div>

                <Pagination
                    size={simulatorController.processMap.size}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    handleChangePage={handleChangePage}
                    handleChangeRowsPerPage={handleChangeRowsPerPage}
                />
                <BottomPanel>
                    <Button onClick={() => appContext.toggleDataView('open')}>
                        Data View
                    </Button>
                    <Button
                        onClick={() =>
                            simulatorController.run().then(() => {
                                appContext.setRerender(!appContext.reRender);
                                openDataView();
                            })
                        }
                    >
                        Run Simulation
                    </Button>
                </BottomPanel>
            </Paper>

            <FormModal
                title={'Configure Simulation'}
                onSubmit={() => {
                    simulatorController
                        .setRandomProcessMap(formData.number_of_processes)
                        .then(processMap => {
                            setProcessMap(processMap);
                            appContext.setIsModalOpen(false);
                        });
                }}
                submitDisabled={
                    formData && !validateInput(formData.number_of_processes)
                }
            >
                <FormGroup column="true">
                    <TextField
                        autoFocus
                        id="number_of_processes"
                        placeholder="Number of Processes"
                        type="number"
                        onChange={e =>
                            isValidInput(e.target.value) &&
                            handleChange(
                                e,
                                'number_of_processes',
                                e.target.value
                            )
                        }
                        value={formData.number_of_processes}
                    />
                </FormGroup>
            </FormModal>
        </TableWrapper>
    );
}
