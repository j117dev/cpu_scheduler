import React, { useState, useContext, useEffect } from 'react';
import styled from 'styled-components';
import { COLORS } from '../theme/colors';
const ipcRenderer = window.require('electron').ipcRenderer;
import Pagination from '../components/table/pagination';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const DataViewWrapper = styled.div`
    -webkit-app-region: drag;
    position: absolute;
    height: 100%;
    width: 100%;
    background: ${COLORS.bluePrimary};
    span {
        position: absolute;
        right: 5px;
        top: 5px;
        cursor: pointer;
    }
    .MuiTablePagination-root {
        margin-top: -20px !important;
        margin-right: 16px;
    }
    .MuiTablePagination-selectRoot {
        display: none;
    }
    .MuiTypography-root {
    }
    .MuiToolbar-root p:first-of-type {
        display: none;
    }
    .MuiPaper-root {
        margin: 24px;
        // margin-top: 0px !important;
        box-shadow: 1px 3px 20px black;
    }
`;

const GanttWrapper = styled.div`
    display: inline-grid;
    grid-template-columns: repeat(10, 1fr);
    grid-auto-columns: min-content;
    width: 100%;
    height: 100px
    padding-left: 16px;
    padding-right: 16px;
    padding-top: 16px;
    div {
        display: grid;
        height: 100%;
    }
    .process-container {
        div {
            display: flex;
            justify-content: center;
            p {
                justify-self: center;
                align-self: center;
            }
        }
        border: 1px solid ${COLORS.grayPrimary}; 
        border-left: .5px solid ${COLORS.grayPrimary};
    label {
        justify-self: right;
    }
`;

const TopPanelWrapper = styled.div`
    display: flex;
    // justify-content: space-between;
    position: absolute;
    top: 112px;
    h5 {
        text-align: center;
        font-size: 14px;
    }
    div {
        padding: 8px;
        color: ${COLORS.grayPrimary};
        background: white;
        box-shadow: 1px 1px 6px black !important;
    }
    div:first-of-type {
    }
    div:nth-of-type(2) {
    }
`;

export default function DataView(props) {
    const rowsPerPage = 10;
    const [page, setPage] = useState(0);
    const [data, setData] = useState({
        processArray: [],
    });

    useEffect(() => {
        ipcRenderer.on('action-update-data', (e, data) => {
            setPage(0);
            setData(data);
        });
    }, []);

    const handleChangePage = (e, val) => {
        setPage(val);
    };

    const generateGanttChart = data.processArray
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((process, i) => (
            <div>
                <div className="process-container">
                    <div>
                        <p>{process.name}</p>
                    </div>
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <label>{process.wait_time}</label>
                    {i === data.processArray.length - 1 &&  (
                        <label style={{position: 'absolute', marginLeft: 70}}>{process.turnaround_time}</label>
                    )}
                </div>
            </div>
        ));

    return (
        <DataViewWrapper>
            <Paper>
                <GanttWrapper>{generateGanttChart}</GanttWrapper>
            </Paper>
            <Pagination
                size={data.processArray.length}
                rowsPerPage={10}
                page={page}
                handleChangePage={handleChangePage}
            />
            <TopPanelWrapper>
                <Paper>
                    <Typography variant="h5">
                        Wait Average: {data.waitAverage}
                    </Typography>
                </Paper>
                <Paper>
                    <Typography variant="h5">
                        Turnaround Average: {data.turnaroundAverage}
                    </Typography>
                </Paper>
            </TopPanelWrapper>
        </DataViewWrapper>
    );
}
