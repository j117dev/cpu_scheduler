export const COLORS = {
    bluePrimary: '#07569C',
    blueSecondary: '#2A4E6E',
    grayPrimary: '#3F464B',
    graySecondary: '#343A40',
    purple: '#7A296B',
    yellow: '#AA9939',
    white: '#ffffff'
}