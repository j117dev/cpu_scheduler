import { FIRST_FIT } from "./memory_manager";

export const FIRST_COME_FIRST_SERVE = 'FIRST_COME_FIRST_SERVE';
export const SHORTEST_JOB_FIRST = 'FIRST_JOB_FIRST';
export const ROUND_ROBIN = 'ROUND_ROBIN';

export const labelMap = {
    [FIRST_COME_FIRST_SERVE]: 'First Come First Serve',
    [SHORTEST_JOB_FIRST]: 'Shortest Job First',
    [ROUND_ROBIN]: 'Round Robin',
    [FIRST_FIT]: 'First Fit'
}