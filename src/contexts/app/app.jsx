import React, { useState, useEffect, useReducer } from 'react';
import { AppContext } from './appContext';
const ipcRenderer = window.require('electron').ipcRenderer;
import SimulatorController from '../../Controllers/SimulatorController';
import { generateGuid } from '../../utils/simulation';
const simulatorController = new SimulatorController();
import * as _ from 'lodash';
const { remote } = window.require('electron');

const defaultModel = {
    total_memory: 0,
    os_memory: 0,
    memory_blocks: [],
    processes: [],
};

/**
 * This function finds the first available block the process will fit into
 * @param {Object} process
 * @param {Array} memory_blocks
 */
const findAvailableBlock = (process, memory_blocks) => {
    for (let i = 0; i < memory_blocks.length; i++) {
        const block = memory_blocks[i];

        if (block.available) {
            if (process.size <= block.size) {
                return { block: block, index: i };
            }
        }
    }

    return false;
};

//get blocks leading up to available block
const getTotalAllocated = (memory_blocks, index) => {
    let pos = 0;
    for (let i = 0; i < memory_blocks.length; i++) {
        pos += memory_blocks[i].size;
        if (i === index - 1) break;
    }
    return pos;
};

/**
 * This function combines empty blocks which are next to eachother
 * @param {Array} memory_blocks
 */
const compactAvailableSpace = memory_blocks => {
    for (let i = 0; i < memory_blocks.length; i++) {
        if (memory_blocks[i].available) {
            //check if theres an empty block to the right
            if (memory_blocks[i + 1] && memory_blocks[i + 1].available) {
                //combine sizes
                memory_blocks[i].size =
                    memory_blocks[i].size + memory_blocks[i + 1].size;
                //remove compacted block
                memory_blocks.splice(i + 1, 1);
            }
        }
    }
    return memory_blocks;
};

const memoryDataReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_MEMORY':
            const availableBlock = findAvailableBlock(
                action.payload,
                state.memory_blocks
            );

            if (availableBlock) {
                const process = Object.assign({}, action.payload, {
                    x: getTotalAllocated(
                        state.memory_blocks,
                        availableBlock.index
                    ),
                });

                //add process to be mapped by view
                state.processes.push(process);

                //add size to mem block array 1 space before available block
                state.memory_blocks.splice(availableBlock.index, 0, {
                    available: false,
                    size: process.size,
                    pid: process.pid,
                });
                state.memory_blocks[availableBlock.index + 1].size =
                    state.memory_blocks[availableBlock.index + 1].size -
                    process.size;
                return Object.assign({}, state, { error: false });
            } 
            else {
                return Object.assign({}, state, { error: true });
            }

        case 'SET_MEMORY':
            state.memory_blocks = [];
            state.memory_blocks.push({
                available: false,
                size: action.payload.os_memory,
            });
            state.memory_blocks.push({
                available: true,
                size: action.payload.total_memory - action.payload.os_memory,
            });
            return Object.assign({}, state, action.payload);
        case 'REMOVE_MEMORY':
            // set available
            state.memory_blocks.find(
                p => p.pid === action.payload
            ).available = true;
            state.memory_blocks.find(p => p.pid === action.payload).pid = '';
            state.processes = _.remove(
                state.processes,
                p => p.pid !== action.payload
            );
            compactAvailableSpace(state.memory_blocks);
            return Object.assign({}, state);
        default:
            return Object.assign({}, state, action.payload);
    }
};

/**
 * @function AppContextProvider Application global data store
 * @param props properties from components higher in the component tree than the provider
 */
export const AppContextProvider = props => {
    //Declare and set default menu state
    const [isMenuOpen, setIsMenuOpen] = useState(true);
    const [activeAlgorithm, setActiveAlgorithm] = useState('');
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [processMap, setProcessMap] = useState({});
    const [reRender, setRerender] = useState(false);

    useEffect(() => {
        simulatorController.setType(activeAlgorithm);
    }, [activeAlgorithm]);

    /**
     * This function will send the set coordinates of x + the height of the
     * main window over IPC to the data view window
     * @param {String} action Open or close the window
     */
    const toggleDataView = action => {
        const coordinates = {
            x: remote.getCurrentWindow().getPosition()[0] + 20,
            y: remote.getCurrentWindow().getPosition()[1] + 20,
        };

        ipcRenderer.send('show-stats-window', { action, coordinates });
    };

    /**
     * This fucntion will send data to the data window process via Electron's IPC
     * @param {Object} data A serialized data object to send to the
     * data view process
     */
    const sendDataToView = data => {
        ipcRenderer.send('send-results-to-stats-window', data);
    };

    const [memoryData, memoryDataDispatch] = useReducer(
        memoryDataReducer,
        defaultModel
    );

    const addMemoryData = payload => {
        memoryDataDispatch({
            type: 'ADD_MEMORY',
            payload,
        });
    };

    const setMemory = payload => {
        memoryDataDispatch({
            type: 'SET_MEMORY',
            payload,
        });
    };

    const removeMemory = payload => {
        memoryDataDispatch({
            type: 'REMOVE_MEMORY',
            payload,
        });
    };

    /**
     * This function will compact the memory and dispatch
     * an update to data reducer
     */
    const compactMemory = () => {
        let { memory_blocks, processes } = memoryData;

        for (let i = 0; i < memory_blocks.length; i++) {
            if (memory_blocks[i].available) {
                //find process to the right of empty block
                if (memory_blocks[i + 1] && memory_blocks[i + 1].pid) {
                    console.log('hur', memory_blocks[i + 1]);
                    //start after empty block and work right, moving processes over until another
                    for (let j = i + 1; j < memory_blocks.length - 1; j++) {
                        //If block is a process, move it over
                        if (!memory_blocks[j].available) {
                            const target = processes.find(
                                p => p.pid === memory_blocks[j].pid
                            );
                            //adjust size of process
                            target.x = target.x - memory_blocks[i].size;
                        }
                    }

                    //move empty block to end of array
                    memory_blocks.push(memory_blocks[i]);
                    //remove empty block from original position
                    memory_blocks.splice(i, 1);
                    //compact available space
                    memory_blocks = compactAvailableSpace(memory_blocks);
                }
            }
        }
        memoryDataDispatch({
            type: 'UPDATE_BLOCKS',
            memory_blocks,
        });
    };

    useEffect(() => {
        console.log('mem', memoryData);
    }, [memoryData]);

    return (
        <AppContext.Provider
            value={{
                isMenuOpen,
                setIsMenuOpen,
                activeAlgorithm,
                setActiveAlgorithm,
                isModalOpen,
                setIsModalOpen,
                processMap,
                setProcessMap,
                toggleDataView,
                sendDataToView,
                simulatorController,
                reRender,
                setRerender,
                setMemory,
                memoryData,
                addMemoryData,
                removeMemory,
                compactMemory,
            }}
        >
            {props.children}
        </AppContext.Provider>
    );
};
