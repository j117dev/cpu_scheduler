import React from 'react';
import { COLORS } from './theme/colors';
import styled from 'styled-components';
import Welcome from './components/welcome/welcome';
import {
    HashRouter,
    Route
  } from "react-router-dom";
import SchedulerContainer from './containers/schedulerContainer';
import DataView from './components/dataView.js';
import { AppContextProvider } from './contexts/app/app';
import MemoryContainer from './containers/memoryContainer';
export default function App() {
    return (
        <HashRouter>
            <div>
                <AppContextProvider>
                        <Route path='/' exact component={Welcome} />
                        <Route path='/cpuScheduler' component={SchedulerContainer} />
                        <Route path='/statWindow' component={DataView} />
                        <Route path='/memoryManager' component={MemoryContainer} />
                </AppContextProvider>
            </div>
        </HashRouter>
    );
}
