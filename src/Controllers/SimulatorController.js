import { FIRST_COME_FIRST_SERVE, SHORTEST_JOB_FIRST } from '../types/cpu_scheduler';
import * as _ from 'lodash';
import { generateGuid, generateRandomBurst } from '../utils/simulation';

export default class SimulatorController {
    constructor() {
        this.type = null;
        this.numberOfProcesses = 0;
        this.processMap = new Map();
    }

    /**
     * This function will clear the state of the simulation controller
     */
    clearSimulation() {
        this.processMap.clear();
        this.numberOfProcesses = 0;
    }

    /**
     * This function will set the simulation type
     * @param {String} type 
     */
    setType(type) {
        this.type = type;
    }

    /**
     * This function will set the number of processes
     * @param {Number} number 
     */
    setNumberOfProcesses(number) {
        this.numberOfProcesses = number;
    }

    /**
     * 
     * @param {String} pid The process ID of the burst to be set 
     * @param {Number} burst The burst to set 
     */
    setBurst(pid, burst) {
        const target = this.processMap.get(pid);

        this.processMap.set(
            pid,
            Object.assign({}, target, { burst: Number(burst) })
        );
    }

    /**
     * This function will set the process map with random burst values
     * @param {*} processCreationCount
     * @returns A promise with the new process map
     */
    setRandomProcessMap() {
        return new Promise(resolve => {
            _.times(this.numberOfProcesses, i => {
                const pid = generateGuid();

                this.processMap.set(pid, {
                    name: `P${++i}`,
                    pid: pid,
                    burst: generateRandomBurst(),
                });
            });
            resolve(this.processMap);
        });
    }

    /**
     * This function will run a simulation based on process type
     * @returns {Map} A new mapping of processes
     */
    run() {
        switch (this.type) {
            case FIRST_COME_FIRST_SERVE:
                return this.runFirstComeFirstServe();
            case SHORTEST_JOB_FIRST:
                return this.runShortestJobFirst();
        }
    }

    /**
     * This function will calculate the wait time and turnaround time for
     * a given map of processes using the first come first serve algorith
     * @param {Map} processMap A mapping of process ID to processes
     * @returns {Map} A new process map;
     */
    runFirstComeFirstServe() {
        let waitTime = 0;

        return new Promise(resolve => {
            Array.from(this.processMap.values()).map((process, i) => {
                //current times
                this.processMap.set(
                    process.pid,
                    Object.assign({}, process, {
                        wait_time: waitTime,
                        turnaround_time: Number(process.burst + waitTime),
                    })
                );
                //set next times
                waitTime = Number(process.burst) + waitTime;
            });

            resolve(this.processMap);
        });
    }

    /**
     * This function will calculate the wait time and turnaround time for
     * a given map of processes, running the shortest job first
     * @param {Map} processMap A mapping of process ID to processes
     * @returns {Map} A new process map;
     */
    runShortestJobFirst() {
        let waitTime = 0;

        return new Promise(resolve => {
            //Sort current map by burst
            const sortedArray = _.orderBy(
                Array.from(this.processMap.values()),
                'burst'
            );
            //clear map
            this.processMap.clear();

            sortedArray.map((process, i) => {
                this.processMap.set(
                    process.pid,
                    Object.assign({}, process, {
                        wait_time: waitTime,
                        turnaround_time: Number(process.burst + waitTime),
                    })
                );
                //set next times
                waitTime = Number(process.burst) + waitTime;
            });
            resolve(this.processMap);
        });
    }

    /**
     * This function will return the averages of the wait time
     * and turnaround time
     */
    getAverages() {
        //convert to array
        const array = Array.from(this.processMap.values());
        //get mean by key
        const wait_time_average = _.meanBy(array, process =>
            Number(process['wait_time'])
        ).toFixed(2);
        const turnaround_time_average = _.meanBy(array, process =>
            Number(process['turnaround_time'])
        ).toFixed(2);

        //round two places
        return {
            wait_time_average,
            turnaround_time_average,
        };
    }
}
